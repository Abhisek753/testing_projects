import React, { useEffect, useState } from 'react';

function InputF() {
  const [name,setName] =useState("")
  const [amount,setAmount] =useState("")
  const [url,setUrl] =useState("")
  const [date,setDate] =useState("")
  const [checked,setChecked] =useState("")
  const [friend,setFriend] =useState("")
  // const [data,setData]=useState([])



  const formContainerStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: '20px',
    border: '1px solid gray', // Added border
    padding: '20px', // Added padding
    width:"300px",
    height:"100%"
  };

  const headingStyle = {
    color: 'blue',
  };

  const inputStyle = {
    margin: '5px',
    padding: '10px',
    border: '1px solid gray',
    borderRadius: '5px',
    width: '200px',
  };
const radioLabelStyle = {
    marginLeft: '5px',
  };

   
 const handleSubmit=()=>{
  const formdata={
    "name":name,
    "amount":amount,
    "url":url,
    "date":date,
    "checked":checked,
    "friend":friend
    }
    console.log("form data---->",formdata)
 }



  return (
    <div  style={{display:"flex",alignItems:"center",justifyContent:"center"}}>
      <div className="formContainer" style={formContainerStyle}>
        <h1 style={headingStyle}>Form</h1>
        <input
          type="text"
          className="formInput"
          style={inputStyle}
          placeholder="ENTER NAME"
          onChange={(e)=>setName(e.target.value)}
        />
        <input
          type="number"
          className="formInput"
          style={inputStyle}
          placeholder="ENTER AMOUNT"
          onChange={(e)=>setAmount(e.target.value)}

        />
        <input
          type="url"
          className="formInput"
          style={inputStyle}
          placeholder="ENTER URL"
          onChange={(e)=>setUrl(e.target.value)}

        />
        <input
          type="date"
          className="formInput"
          style={inputStyle}
          placeholder="ENTER"
          onChange={(e)=>setDate(e.target.value)}

        />
       <div>
         <input
          type="checkbox"
          className="formInput"
          style={inputStyle}
          placeholder="ENTER"
          onChange={(e)=>setChecked(e.target.value)}

        />
          <label style={radioLabelStyle}>Checked</label>

       </div>
        
        <button style={{backgroundColor:"blue",color:"wheat"}}  onClick={handleSubmit} >Submit</button>
        
      </div>
    </div>
  );
}

export default InputF;