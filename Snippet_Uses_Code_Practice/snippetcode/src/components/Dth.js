import React, { useState } from 'react'
import {Button, Col, Form,FormGroup,Input, Label, Row} from "reactstrap"
import 'bootstrap/dist/css/bootstrap.min.css';

const Dth = () => {
  const [minvalue,setMinvalue]=useState("")
  const [maxvalue,setMaxvalue]=useState("")
  const [discounttype,setDiscounttype]=useState("")
  const [discountvalue,setDiscountvalue]=useState("")
  const [minError,setMinError]=useState("")
  const [maxError,setMaxError]=useState("")




  const handleSubmit=(e)=>{
      e.preventDefault()
      if(minvalue<11 ){
        setMinError("Please Enter value more than 10")
      }else if(maxvalue>99 || maxvalue<=minvalue){
        setMaxError("Please Enter value less than 99 and greater than min value")
         
      }else{
        const formdata={
          minvalue:minvalue,
          maxvalue:maxvalue,
          discounttype:discounttype,
          discountvalue:discountvalue
        }
        setMinError("")
        setMaxError("")
        
       console.log(formdata);
       alert(JSON.stringify(formdata));

      }
      

  }

  return (
    <div>
     
      <Form onSubmit={handleSubmit}>
  <Row className="row-cols-lg-auto g-3 align-items-center">
    <Col>
      <Label
        className="min"
        for="min"
      >
       Min Value
      </Label>
      <Input
        id="min"
        name="min"
        placeholder="min amount"
        type="number"
        onChange={(e)=>setMinvalue(e.target.value)}
      />
      <Label>{minError}</Label>
    </Col>
    <Col>
      <Label
        className="max"
        for="max"
      >
         Max Value
      </Label>
      <Input
        id="max"
        name="max"
        placeholder="max amount"
        type="number"
        onChange={(e)=>setMaxvalue(e.target.value)}

      />
      <Label color='text-danger' >{maxError}</Label>
    </Col>
    <Col>
      <Label
        className="discount"
        for="discount"
      >
       Min Value
      </Label>
      <Input
        id="discount"
        name="discount"
        placeholder="discount"
        type="select"
        onChange={(e)=>setDiscounttype(e.target.value)}
      >
        <option>Select Discount Type</option>
        <option value="Percentage" >Percentage</option>
        <option value="Rupees" >Rupees</option>

      </Input>
      
    </Col>
    <Col>
      <Label
        className="discountvalue"
        for="discountvalue"
      >
         Discountvalue
      </Label>
      <Input
        id="discountvalue"
        name="discountvalue"
        placeholder="Discount Amount"
        type="number"
        onChange={(e)=>setDiscountvalue(e.target.value)}

      />
      
    </Col>
    <Col>
      <FormGroup check>
        <Input
          id="exampleCheckbox"
          name="checkbox"
          type="checkbox"
        />
        <Label
          check
          for="exampleCheckbox"
        >
          Remember Me
        </Label>
      </FormGroup>
    </Col>
    <Col>
      <Button>
        Submit
      </Button>
    </Col>
  </Row>
</Form>

      
    </div>
  )
}

export default Dth