import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Skill from "./components/Skill/Mainpage/Index"
import Appraisal from "./components/Appraisal/Mainpage/Index"
import Home from "./components/Index"
import AppraisalList from "./components/Appraisal/AppraisalList/Index"
import EditAppraisalList from "./components/Appraisal/EditAppraisalForm/Index"
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
     {/* <MainPage/> */}
     <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/skill" element={<Skill/>}/>
        <Route path="/Appraisal" element={<Appraisal/>}/>
        <Route path="/Appraisallist" element={<AppraisalList/>}/>
        <Route path="/EditAppraisal/:id" element={<EditAppraisalList/>}/>


         
         
      
      </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
