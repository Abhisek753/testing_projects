import React from 'react'
import Sidebar from "../GlobalComponent/Sidebar/Index";



const Index = ({children}) => {
    return (

        <div className='layoutcontainer'>
            <div className='sidebarcontainer' >
                <Sidebar />
            </div>
            <div className='containtbar'>
               {children}
            </div>
        </div>

    )
}

export default Index
