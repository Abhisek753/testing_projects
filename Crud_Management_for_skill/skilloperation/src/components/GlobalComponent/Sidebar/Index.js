import React from 'react';
import { NavItem, Nav } from 'reactstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faUser, faArrowRight, faListUl } from '@fortawesome/free-solid-svg-icons';
// import '../Assets/css/sidebar.css';

const Index = () => {
  return (
    <div className="sidebar">
      <Nav vertical>
        <NavItem>
          <Link to="/" className="nav-link">
            <FontAwesomeIcon icon={faHome} className="icon" />
            <span> Home</span>
          </Link>
        </NavItem>
        <NavItem>
          <Link to="/skill" className="nav-link">
            <FontAwesomeIcon icon={faUser} className="icon" />
            <span> Skill</span>
            
          </Link>
        </NavItem>
        <NavItem>
          <Link to="/appraisal" className="nav-link">
            <FontAwesomeIcon icon={faArrowRight} className="icon" />
            <span>Appraisal</span>
          </Link>
        </NavItem>
        <NavItem>
          <Link to="/Appraisallist" className="nav-link">
            <FontAwesomeIcon icon={faListUl} className="icon" />
            <span>Appraisallist</span>
          </Link>
        </NavItem>
      </Nav>
    </div>
  );
};

export default Index;
