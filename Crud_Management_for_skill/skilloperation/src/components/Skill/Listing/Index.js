import React, { useEffect, useState } from "react";
import {
  Button,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Table,
} from "reactstrap";
// import { URL } from "../../../config.js";

import Select from "react-select";
import { deleteSkillsServices, getSkillTypesService, getSkillsService, saveSkillsService } from "../../../services/addSkills.services.js";


const Index = () => {
  const [data, setData] = useState({
    skill_name: "",
    skillTypeId: "",
  });
  const [addNewModal, setAddNewModal] = useState(false);
  const [listingdata, setListingData] = useState([]);
  const [option,setOption]=useState([])
  // const [selectedSkillType, setSelectedSkillType] = useState(null);
   
 

  const handleChange = (name, val) => {
    console.log(val)
    setData({ ...data, [name]: val });
  };

  const handleSubmit=async()=>{
  
    saveSkillsService(data).then((response) => {
    console.log(response);
   setData({ skill_name: "", skillTypeId: "" });
   setAddNewModal(false);
  })
  .catch((error) => {
    console.error('Error:', error);
  });
  }

const handleModalToggle = () => {
    setAddNewModal(!addNewModal);
  };
  
  const getData=async()=>{
  
//srvices file
 getSkillsService().then((response) => {
  console.log(response,"<======get skills")
  setListingData(response.data.data)
})
.catch((error) => {
  console.error('Error:', error);
});
}

const getSkillType=async()=>{
  getSkillTypesService().then((response) => {
   setOption(response.data.data)
  })
  .catch((error) => {
    console.error('Error:', error);
  });
  }

  const handleDelete=async(id)=>{
    deleteSkillsServices(id).then((res)=>{
      console.log("skill deleted",res,)
      getData();

    }).catch((error)=>{
      console.log(error)
    })
   
    await setListingData((prevData) => prevData.filter(item => item.id !== id));
  }

const skillTypes = option.map(option => ({
    value: option.skilltypes._id,
    label: option.skilltypes.skill_type,
  }));
  useEffect(() => {
    getData();
    getSkillType();
  }, [data]);


  return (
    <div>
      <div style={{display:"flex",justifyContent:"flex-end",padding:"20px"}}>
        <Button className="btn btn-primary" onClick={handleModalToggle}>
          Add new skill
        </Button>
      </div>
      <div className="offset">
        <Table hover className="skilllist">
          <thead>
            <tr>
              <th>Number</th>
              <th>Skill</th>
              <th>Type</th>
              <th>Action</th>

            </tr>
          </thead>
          <tbody >
            {listingdata.length>0&&listingdata.map((item,i)=>(
            <tr>
              <th scope="row">{i+1}</th>
              <td>{item.skills.skill_name}</td>
              <td>{item.skills.skillTypeId.skill_type}</td>
              
              <td 
              onClick={()=>handleDelete(item.skills._id)}
               >
                <i class="fa-solid fa-xmark"></i>
               </td>

            </tr>
            ))}
          </tbody>
        </Table>
      </div>

      <div>
        <Modal size="md" isOpen={addNewModal}>
          <ModalHeader>Add New Skill</ModalHeader>
          <ModalBody>
            <form className="">
              <div className="form-group">
                <label>Skill Name</label>
                <Input
                  type="text"
                  value={data.skill}
                  onChange={(e) => handleChange("skill_name", e.target.value)}
                />
              </div>
               
              <Select
                  onChange={(e)=>handleChange("skillTypeId",e.value)}
                  // value={selectedSkillType}
                  options={skillTypes}
                />

            </form>
          </ModalBody>
          <ModalFooter>
            <div className="">
              <button
                className="btn btn-secondary mr-3"
                onClick={handleModalToggle}
              >
                Cancel
              </button>
              <button className="btn btn-primary  ml-3" onClick={handleSubmit}>
                Submit
              </button>
            </div>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
};

export default Index;
