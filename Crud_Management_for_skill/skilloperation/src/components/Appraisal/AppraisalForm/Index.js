import React, { useEffect, useState } from 'react'
import "../../../Assets/css/style.css"
import { Button, Input, Table } from 'reactstrap'
import Select from "react-select";

import {getSkillTypesService, getSkillsService, saveAppraisalDataServices } from '../../../services/addSkills.services'
import "../../../Assets/css/style.css"

const Index = () => {
  const userId="64468760277a8578a30951c5"
  const [data, setData] = useState([]);
  const [selectedSkills, setSelectedSkills] = useState([]);
  const [option, setOption] = useState([]);
  const [skillList, setSkillList] = useState([]);
  const [checkedSkills, setCheckedSkills] = useState({});
  const [skillsData, setSkillData] = useState([])
  const [remark,setRemarks]=useState("")
 
  const getSkillType = async () => {
    getSkillTypesService().then((response) => {
      setOption(response.data.data)
    })
      .catch((error) => {
        console.error('Error:', error);
      });
  }
  useEffect(() => {
    getSkillType()
  }, [])

  const skillTypes = option?.map(option => ({
    type: option.skilltypes.skill_type,
  }));

  const handleSkillList = () => {
    const res = data && data.map((option) => ({
      value: option.skills._id,
      label: option.skills.skill_name,
      type: option.skills.skillTypeId.skill_type
    })).filter((skill) => !selectedSkills.some((selectedSkill) => selectedSkill.value === skill.value));
    setSkillList(res);
  }
  useEffect(() => {
    handleSkillList()
  }, [data, selectedSkills])


  const getData = async () => {

    getSkillsService().then((response) => {
      setData(response.data.data)
    })
      .catch((error) => {
        console.error('Error:', error);
      });
  }

  const DeleteSelectedData = (skillToDelete) => {

    const updatedSelectedSkills = selectedSkills.filter(
      (skill) => skill.value !== skillToDelete.value
    );

    setSelectedSkills(updatedSelectedSkills);
    setSkillList((prevSkillList) => [...prevSkillList, skillToDelete]);
  }



  const handleChecked = (e, skill, level) => {
    const updatedCheckedSkills = { ...checkedSkills };
    updatedCheckedSkills[skill.value] = [level];

    const newSkillsData = [];
    for (const skillId in updatedCheckedSkills) {
      newSkillsData.push({
        userId: userId,
        skillId,
        level: updatedCheckedSkills[skillId][0]
      });
    }
    setCheckedSkills(updatedCheckedSkills);
    setSkillData(newSkillsData);
  };

  const saveDataToApi = () => {
    saveAppraisalDataServices({
      "userId":userId,
      "remarks":remark,
      "skillsData":skillsData})
    .then((response) => {
      console.log(response);
      setSelectedSkills([]);
      setRemarks("")
      setCheckedSkills("")
    })
      .catch((error) => {
        console.error('Error:', error);
      });
  };
  
  useEffect(() => {
    getData()

  }, [])

  return (
    <div>
      <div className='boxlistContainer'>
        <div>
          {skillTypes?.map((type, i) => (
            <div key={i} className='appraisal-listing' >
              <div>
                <h4>{type.type}</h4>
              </div>
              <div>
                <Table hover className='appraisal-form-skilllist'>
                  <thead>
                    <tr>
                      <th >Skill</th>
                      <th>P1 - Entry</th>
                      <th>P2 - Intermediate</th>
                      <th>P3 - Specialist</th>
                      <th>P4 - Master</th>
                      <th>P5 - Expert</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {console.log(selectedSkills,"<===selectedSkills")}
                    {selectedSkills
                      ?.filter(skill => skill.type === type.type)
                      .map((skill, index) => (
                        <tr key={index}>
                          <th scope="row">{skill.label}</th>
                          <td>
                            <Input
                              type="radio"
                              value="P1-Entry"
                              checked={checkedSkills[skill.value]?.includes("P1-Entry")}
                              onChange={(e) => handleChecked(e, skill, "P1-Entry")}
                            />
                          </td>
                          <td>
                            <Input
                              type="radio"
                              value="P2-Intermediate"
                              checked={checkedSkills[skill.value]?.includes("P2-Intermediate")}
                              onChange={(e) => handleChecked(e, skill, "P2-Intermediate")}
                            />
                          </td>
                          <td>
                            <Input
                              type="radio"
                              value="P3-Specialist"
                              checked={checkedSkills[skill.value]?.includes("P3-Specialist")}
                              onChange={(e) => handleChecked(e, skill, "P3-Specialist")}
                            />
                          </td>
                          <td>
                            <Input
                              type="radio"
                              value="P4-Master"
                              checked={checkedSkills[skill.value]?.includes("P4-Master")}
                              onChange={(e) => handleChecked(e, skill, "P4-Master")}
                            />
                          </td>
                          <td>
                            <Input
                              type="radio"
                              value="P5-Expert"
                              checked={checkedSkills[skill.value]?.includes("P5-Expert")}
                              onChange={(e) => handleChecked(e, skill, "P5-Expert")}
                            />
                          </td>
                          <td onClick={() => DeleteSelectedData(skill)}>
                          <i class="fa-solid fa-xmark"></i>
                          </td>
                        </tr>
                      ))}
                  </tbody>
                </Table>
              </div>
              <div>
                <div className='select-box'  >
                  <Select
                    onChange={(e) => { setSelectedSkills([...selectedSkills, e]) }}
                    options={skillList.filter((skill) => skill.type === type.type)} />
                </div>
              </div>
            </div>
          ))}
        </div>
        <div>
        <Input type='textarea' onChange={(e)=>setRemarks(e.target.value)} />
        </div>
        <div>
          <Button color="primary" onClick={saveDataToApi}>Submit</Button>
        </div>
      </div>
    </div>
  )
}

export default Index