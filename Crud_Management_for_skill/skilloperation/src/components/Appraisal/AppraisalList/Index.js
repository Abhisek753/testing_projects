import React, { useEffect, useState } from 'react'
import "../../../Assets/css/style.css"
import Layout from "../../Layout/Index"
import { deleteAppraisalsServices, getAppraisalListServices } from '../../../services/addSkills.services'
import { Table } from 'reactstrap'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom'
const Index = () => {
  const [appraisaldata, setAppraisalData] = useState([])
  const [dropdownOpen, setDropdownOpen] = useState(Array(appraisaldata.length).fill(false));
  var listingdata
  const getSavedAppraisalSkills = () => {
    getAppraisalListServices().then((res) => {
      // console.log(res.data.data)
      setAppraisalData(res.data.data)
    })
      .catch((err) => {
        console.log(err);
      })
  }

  //date conversion 
  function formatTimestamp(timestamp) {
    const date = new Date(timestamp);
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const ampm = date.getHours() >= 12 ? "PM" : "AM";

    const hours = date.getHours() % 12 || 12;

    const day = date.getDate();
    const month = months[date.getMonth()];
    const year = date.getFullYear();
    const minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    const formattedTimestamp = `${month} ${day}, ${year}, ${hours}:${minutes} ${ampm}`;

    return formattedTimestamp;
  }
  const toggleDropdown = (index) => {

    const newDropdownOpen = [...dropdownOpen];
    newDropdownOpen[index] = !newDropdownOpen[index];
    setDropdownOpen(newDropdownOpen);
  }
  const handleAppraisalDelete = (id) => {
    deleteAppraisalsServices(id).then((res) => {
      console.log(res, "deleted appraisalList");
      getSavedAppraisalSkills();


    }).catch((error) => {
      console.log(error)
    })
  }

  useEffect(() => {
    getSavedAppraisalSkills()
  }, [])
  return (
    <Layout>
      <div className='page-heading'>
        <h4>
          AppraisalList

        </h4>
      </div>
      <div>
        <Table hover className="skilllist">
          <thead>
            <tr>
              <th>Number</th>
              <th>Name</th>
              <th>Created</th>
              <th>Remarks</th>
              <th>Action</th>


            </tr>
          </thead>
          <tbody >
            {appraisaldata.length > 0 && appraisaldata.map((item, i) => (
              <tr key={i}>
                <th scope="row">{i + 1}</th>
                <td>{item.appraisals.userId.first_name}</td>
                <td>{formatTimestamp(item.appraisals.createdAt)}</td>
                <td>{item.appraisals.remarks}</td>
                <td >
                  <Dropdown isOpen={dropdownOpen[i]} toggle={() => toggleDropdown(i)}>
                    <DropdownToggle>
                      <span className="dots">...</span>
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem >
                         <Link to={`/EditAppraisal/${item.appraisals._id}`}>Edit</Link>
                      </DropdownItem>
                      <DropdownItem onClick={() => handleAppraisalDelete(item.appraisals._id)} >Delete</DropdownItem>
                    </DropdownMenu>
                  </Dropdown>
                </td>

              </tr>
            ))}
          </tbody>
        </Table>

      </div>
    </Layout>
  )
}

export default Index