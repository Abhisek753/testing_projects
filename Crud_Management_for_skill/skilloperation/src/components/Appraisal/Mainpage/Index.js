import React from 'react'
import BoxList from "../AppraisalForm/Index" 
import "../../../Assets/css/style.css"
import Layout from "../../Layout/Index"

const Index = () => {
  return (
    <Layout>
            <div className='appraisalContainer'>
                <BoxList/>
            </div>
    </Layout>
  )
}

export default Index