import axios from "axios";
import { SKILLTYPE } from "../config";

export const saveSkillsService=(data)=>{
    const axiosConfig = {
        method: 'post',
        url: SKILLTYPE+"saveSkill",
        data:data,
        headers: {
          'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQ0Njg3NjAyNzdhODU3OGEzMDk1MWM1IiwidXNlcl9yb2xlIjoiQWRtaW4iLCJpYXQiOjE2OTc0MzcwMzEsImV4cCI6MTY5Nzg2OTAzMX0.1FDL6Bra62FgOOv-XuiISkCzDjbG43N6lBXRHQF_07g", // Set the Authorization header with the token
          'Content-Type': 'application/json', 
        },
      };
     return axios(axiosConfig)
    

}

export const getSkillsService=()=>{
    const axiosConfig = {
        method: 'post',
        url: SKILLTYPE+"getSkills",
        data: {},
        headers: {
          'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQ0Njg3NjAyNzdhODU3OGEzMDk1MWM1IiwidXNlcl9yb2xlIjoiQWRtaW4iLCJpYXQiOjE2OTc0MzcwMzEsImV4cCI6MTY5Nzg2OTAzMX0.1FDL6Bra62FgOOv-XuiISkCzDjbG43N6lBXRHQF_07g", // Set the Authorization header with the token
          'Content-Type': 'application/json', 
        },
      };
     return axios(axiosConfig)
}
export const getSkillTypesService=()=>{
  const axiosConfig = {
    method: 'post',
    url: SKILLTYPE+"getSkillTypes",
    data: {},
    headers: {
      'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQ0Njg3NjAyNzdhODU3OGEzMDk1MWM1IiwidXNlcl9yb2xlIjoiQWRtaW4iLCJpYXQiOjE2OTc0MzcwMzEsImV4cCI6MTY5Nzg2OTAzMX0.1FDL6Bra62FgOOv-XuiISkCzDjbG43N6lBXRHQF_07g", // Set the Authorization header with the token
      'Content-Type': 'application/json', 
    },
  };
  return axios(axiosConfig)
}

export const saveAppraisalDataServices=(data)=>{
  const axiosConfig = {
    method: 'post',
    url: SKILLTYPE + "saveAppraisalData",
    data:data,
    headers: {
      'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQ0Njg3NjAyNzdhODU3OGEzMDk1MWM1IiwidXNlcl9yb2xlIjoiQWRtaW4iLCJpYXQiOjE2OTc0MzcwMzEsImV4cCI6MTY5Nzg2OTAzMX0.1FDL6Bra62FgOOv-XuiISkCzDjbG43N6lBXRHQF_07g", 
      'Content-Type': 'application/json',
    },
  };
 return axios(axiosConfig)
}

export const getAppraisalListServices=()=>{
  const axiosgetConfig = {
    method: 'post',
    url: SKILLTYPE + "getAppraisalList",
    data: {
      userId:"64468760277a8578a30951c5"
    },
    headers: {
      'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQ0Njg3NjAyNzdhODU3OGEzMDk1MWM1IiwidXNlcl9yb2xlIjoiQWRtaW4iLCJpYXQiOjE2OTc0MzcwMzEsImV4cCI6MTY5Nzg2OTAzMX0.1FDL6Bra62FgOOv-XuiISkCzDjbG43N6lBXRHQF_07g", 
      'Content-Type': 'application/json',
    },
  };
  return axios(axiosgetConfig)
}

export const deleteSkillsServices=(id)=>{
  console.log(id)
  const axiosgetConfig = {
    method: 'post',
    url: SKILLTYPE + "deleteSkill",
    data: {
      skillId:id
    },
    headers: {
      'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQ0Njg3NjAyNzdhODU3OGEzMDk1MWM1IiwidXNlcl9yb2xlIjoiQWRtaW4iLCJpYXQiOjE2OTc0MzcwMzEsImV4cCI6MTY5Nzg2OTAzMX0.1FDL6Bra62FgOOv-XuiISkCzDjbG43N6lBXRHQF_07g", 
      'Content-Type': 'application/json',
    },
  };
  return axios(axiosgetConfig)

}

export const deleteAppraisalsServices=(id)=>{
  console.log(id)
  const axiosgetConfig = {
    method: 'post',
    url: SKILLTYPE + "deleteAppraisalData",
    data: {
      appraisalId:id
    },
    headers: {
      'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQ0Njg3NjAyNzdhODU3OGEzMDk1MWM1IiwidXNlcl9yb2xlIjoiQWRtaW4iLCJpYXQiOjE2OTc0MzcwMzEsImV4cCI6MTY5Nzg2OTAzMX0.1FDL6Bra62FgOOv-XuiISkCzDjbG43N6lBXRHQF_07g", 
      'Content-Type': 'application/json',
    },
  };
  return axios(axiosgetConfig)

}

export const getOneAppraisalServices=(id)=>{
  const axiosgetConfig = {
    method: 'post',
    url: SKILLTYPE + "getOneAppraisal",
    data: {
      appraisalId:id
    },
    headers: {
      'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQ0Njg3NjAyNzdhODU3OGEzMDk1MWM1IiwidXNlcl9yb2xlIjoiQWRtaW4iLCJpYXQiOjE2OTc0MzcwMzEsImV4cCI6MTY5Nzg2OTAzMX0.1FDL6Bra62FgOOv-XuiISkCzDjbG43N6lBXRHQF_07g", 
      'Content-Type': 'application/json',
    },
  };
  return axios(axiosgetConfig)
}

export const updateAppraisalDataServices=(data)=>{
  const axiosConfig = {
    method: 'post',
    url: SKILLTYPE + "updateAppraisalData",
    data:data,
    headers: {
      'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQ0Njg3NjAyNzdhODU3OGEzMDk1MWM1IiwidXNlcl9yb2xlIjoiQWRtaW4iLCJpYXQiOjE2OTc0MzcwMzEsImV4cCI6MTY5Nzg2OTAzMX0.1FDL6Bra62FgOOv-XuiISkCzDjbG43N6lBXRHQF_07g", 
      'Content-Type': 'application/json',
    },
  };
 return axios(axiosConfig)
}