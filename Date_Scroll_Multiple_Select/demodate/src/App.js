import React, { useEffect, useState } from 'react';

import moment from 'moment';
import "react-scroll-calendar/build/react-scroll-calendar.css";
import './App.css';

function App() {
  const [date, setDate] = useState(moment('2019-01-23', 'YYYY-MM-DD'))
  const [data, setData] = useState([])
  const scrollToTop = (event) => {
    event.preventDefault();
    const dateContainer = document.querySelector(".date-container");
    if (dateContainer) {
      dateContainer.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  useEffect(() => {
    let ans = []
    for (let i = 0; i < 20; i++) {
      ans.push(i)
    }
    setData(ans)
  }, [])

  return (
    <div className="App">
      <div className="parent" style={{ border: "3px solid red", display: "flex", overflow: "auto", height: "9000px" }}>
        <div className="date-container" id="dateContainer" style={{ border: "3px solid teal", height: "500px", width: "500px", margin: "20px", overflow: "auto" }}>
          <div className="date">
            {
              data.map((e, i) => (
                <div style={{ height: "100px", width: "100px" }}>{`box${i}`}</div>
              ))
            }
          </div>
          <a href="#dateContainer" onClick={scrollToTop}>Scroll to Top</a>
        </div>
        <div className="textdemo-container" style={{ border: "3px solid black", width: "100%", margin: "20px", overflow: "auto" }}>
          
        </div>
      </div>
    </div>
  );
}

export default App;
