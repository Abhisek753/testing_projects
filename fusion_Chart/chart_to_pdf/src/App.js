import './App.css';
import Fusion from './Fusion';

function App() {
  return (
    <div className="App">
     <h2>Fusiion Chart</h2>
     <Fusion/>
    </div>
  );
}

export default App;
